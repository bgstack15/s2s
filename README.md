# README for shortcut2symlink project
## Overview
A Windows client is used to maintain a website/ subdirectory of files that will be processed using the [gallery](https://gitlab.com/bgstack15/gallery) project. To facilitate the process of preparing files for this website/ subdirectory, particularly over a wireless network connection, users have been instructed to just make Windows-style shortcuts (using CTRL+SHIFT dragging) in the website subdirectory in whatever layout desired.

This project includes components that will scan the designated directory for .lnk files anywhere underneath, and interpret their targets, and generate symlinks in place of the .lnk files.

The point of using symlinks in this case is to reduce the disk usage and number of duplicate files and directories. To SMB users on the Windows client, the symlink files will look like copies, but that is acceptable because the main focus is the gallery generation for the web server.

## shortcut2symlink upstream
[s2slib](https://gitlab.com/bgstack15/s2slib)

## Alternatives
This appears to be original work with no alternatives. I have not found other places on the Internet where a user wants to interpret a .lnk file and substitute with a Linux-style symbolic link.

## Reason for existence
Reduce disk space by removing duplicate files, while still having a whole directory structure readable by the gallery solution for generating static site pages.

## How to use
On a Devuan Ceres system with nfs access to the filesystem, run s2s-inotify.sh after configuring its watchfile settings and commands.

    /mnt/public/Support/Programs/shortcuts/s2s-inotify.sh /mnt/public/Images/username1-photos/website 2>&1 | tee -a /mnt/public/Support/Programs/shortcuts/log1

## Dependencies
Distro          | Packages
--------------- | -----------------------------
Devuan Ceres    | python3-liblnk, inotify-tools

## References
[.lnk format reference](https://github.com/libyal/liblnk/blob/main/documentation/Windows%20Shortcut%20File%20(LNK)%20format.asciidoc)

## Differences from upstream
N/A
