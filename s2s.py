#!/usr/bin/env python3
# File: s2s.py
# Locations:
#    /mnt/public/Support/Programs/shortcuts/
#    https://gitlab.com/bgstack15/s2s
# Author: bgstack15@gmail.com
# SPDX-License-Identifier: GPL-3.0
# Startdate: 2021-02-11 20:00
# Title: Shortcut To Symlink cli
# Project: shortcut to symlink (s2s)
# Purpose: Convert .lnk files to POSIX symlinks within a single filesystem
# History:
# Usage: ./s2s.py --help
# Reference:
#    generate.py from /mnt/public/Support/Programs/gallery
# Improve:
# Documentation:
# Dependencies:

from argparse import ArgumentParser
from s2slib import *
s2s_version = "2021-02-12a"

parser = ArgumentParser(description="Convert .lnk shortcuts to symlinks on a SMB share")
parser.add_argument("-d","--debug",nargs='?', default=0, type=int, choices=range(0,11), help="Set debug level")
parser.add_argument("-v","--version", action="version", version="%(prog)s " + s2s_version)
g_dryrun = parser.add_mutually_exclusive_group()
g_dryrun.add_argument("-n","--dryrun", action="store_true", help="Make no changes (default)")
g_dryrun.add_argument("-a","--apply", action="store_true", help="Actually make changes")
g_delete = parser.add_mutually_exclusive_group()
g_delete.add_argument("-D","--delete", action="store_true",default=False, help="Delete .lnk if symlink is successful")
g_delete.add_argument("-N","--nodelete","--no-delete", action="store_true", default=True, help="Do not delete .lnk")
parser.add_argument("-i","--indir",required=True)

# pull useful values out of the argparse entry
args = parser.parse_args()
debuglevel=0
if args.debug is None:
   debuglevel = 10 # if -d without a number, then 10
elif args.debug:
   debuglevel = args.debug
indir = args.indir
dryrun = args.dryrun or not args.apply # the mutually exclusive group handles this OK
delete = args.delete or not args.nodelete # the mutually exclusive group handles this OK
eprint(args)
eprint(f"dryrun {dryrun}")
eprint(f"delete {delete}")
eprint(f"debuglevel {debuglevel}")

replace_all_symlinks(indir, delete = delete, debuglevel = debuglevel, dryrun = dryrun)
